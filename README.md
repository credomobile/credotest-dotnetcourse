## Instructions

Please read these instructions carefully and follow them the best you can. If you get stuck email us--there's a ```mailto:``` link at the bottom of these instructions to do that.

## About the application - Course Registration

Please build API for student to register for a course with below business logic

1. As a student, I can enroll up to 3 courses.
2. Course can allow only up to 15 students.

### How to start

1. Fork this repository into your own Bitbucket branch.
2. Solve the problem below, checking in to your branch as you go as if this were a real project you're working on. Note that we're also testing your familiarity with Bitbucket/Git. We will look at your commit history closely.
3. Submit a pull request back to this repository with your solution when finished.

### Architecture

1. Implement the API using .net, .net core, web API. 
2. Make sure you write unit tests for the code.
3. Persist data however you want but that is not important part of the test. We are more interested to see business logic mentioned above and unit tests associated to the same
4. Doing CRUD on Student and/or Course is optional.
5. If you are able to add UI, that is great, but its optional.


Please email questions to: code@credomobile.com. Good Luck.